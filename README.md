# DEV: Transportoppdrag Deploy Repository

This is the GitOps / DevOps repository for RuterKontroll  applications deployed in `DEV` environment.

For details on how to use this repository, see
[deploy pipeline documentation](https://gitlab.com/ruter-as/taas/gitops/cicd-common/-/blob/master/doc/deploy-pipeline.md).
